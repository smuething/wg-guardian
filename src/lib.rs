pub mod wg_guardian {

use std::error;
use std::fmt;
use std::net::{IpAddr, SocketAddr};
use rusqlite::{Connection, NO_PARAMS, params, OpenFlags};
use log::{info,debug};


pub struct Peer {
    pub interface: String,
    pub pub_key: String,
    pub endpoint: SocketAddr,
    pub last_handshake: i64,
    pub received: i64,
    pub sent: i64,
}

pub struct Interface {
    pub name: String,
    pub pub_key: String,
    pub endpoint: SocketAddr,
    pub peers: Vec<Peer>,
}

impl fmt::Debug for Interface {

    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.debug_struct("Interface")
            .field("name", &self.name)
            .field("pub_key", &self.pub_key)
            .field("endpoint", &self.endpoint)
            . finish()
    }

}

const DB_FILE : &str = "data.sqlite";

pub fn open_database() -> rusqlite::Result<Connection> {

    if let Some(conn) = Connection::open_with_flags(
        DB_FILE,
        OpenFlags::SQLITE_OPEN_READ_WRITE)
        .ok() {
            info!("Opened database file {}",DB_FILE);
            return Ok(conn);
        }

    let mut conn = Connection::open(DB_FILE)?;
        
    info!("Created new database file {}",DB_FILE);

    conn.execute_batch(
        "CREATE TABLE interfaces (
            name TEXT PRIMARY KEY,
            pub_key TEXT NOT NULL,
            ip TEXT NOT NULL,
            port INT NOT NULL
            ) WITHOUT ROWID;

         CREATE TABLE peers (
            pub_key TEXT PRIMARY KEY,
            interface TEXT NOT NULL,
            endpoint_ip TEXT,
            endpoint_socket INT,
            last_handshake INT,
            received INT,
            sent INT
            ) WITHOUT ROWID;
         CREATE INDEX peer_interface ON peers(interface);
         CREATE INDEX peer_endpoint_ip ON peers(endpoint_ip);"
    )?;

    debug!("Initialized database schema");

    let tx = conn.savepoint()?;

    {
        let mut stmt = tx.prepare_cached("INSERT INTO interfaces (name, pub_key, ip, port) VALUES (?1, ?2, ?3, ?4)")?;

        stmt.insert(params!["wg0","PUBKEY","10.1.2.3",1911])?;
        stmt.insert(params!["wg1","PUBKEY1","10.1.2.4",1911])?;
        stmt.insert(params!["wg2","PUBKEY2","10.1.2.5",1911])?;
    }
        
    tx.commit()?;

    debug!("Wrote initial DB data");

    Ok(conn)
}

pub fn use_database(conn: &Connection) -> Result<(),Box<error::Error>> {

    conn.query_row(
        "SELECT COUNT(*) FROM peers",
        NO_PARAMS,
        |row| Ok(println!("Result: {}",row.get::<_,i32>(0)?))
    )?;

    let mut stmt = conn.prepare_cached("SELECT * FROM interfaces")?;

    stmt.query_and_then(
        NO_PARAMS,
        |row| -> Result<_,Box<error::Error>> { Ok(Interface{
            name: row.get(0)?,
            pub_key: row.get(1)?,
            endpoint: SocketAddr::from((row.get::<_,String>(2)?.parse::<IpAddr>()?,row.get(3)?)),
            peers: Vec::new(),
        })})?.for_each(|i| {println!("{:?}",i.unwrap());});

    Ok(())
}

}
