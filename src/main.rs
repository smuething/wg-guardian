use std::process::{Command,exit};
use std::net::{IpAddr, SocketAddr};

use pretty_env_logger;

use wg_guardian::wg_guardian::*;


fn main() {

    pretty_env_logger::init();

    let conn = open_database().unwrap();

    use_database(&conn).unwrap();

    let output = match Command::new("wg")
        .arg("show")
        .arg("all")
        .arg("dump")
        .output() {
            Ok(output) => output,
            Err(_) => {
                println!("Error, aborting!");
                exit(1);
            },
        };

    let output = String::from_utf8(output.stdout).expect("foo");

    let mut iface = "";

    let mut interfaces : Vec<Interface> = Vec::new();

    for line in output.lines() {
        let cols : Vec<&str> = line.split_ascii_whitespace().collect();

        if iface != cols[0] {
            interfaces.push(Interface{
                name: cols[0].to_string(),
                pub_key: cols[1].to_string(),
                endpoint: SocketAddr::from(("127.0.0.1".parse::<IpAddr>().unwrap(),cols[3].parse().unwrap())),
                peers: Vec::new(),
            });

            iface = cols[0];

            continue;
        }

        interfaces.last_mut().unwrap().peers.push(Peer{
            interface: iface.to_string(),
            pub_key: cols[1].to_string(),
            endpoint: cols[3].to_string().parse().unwrap(),
            last_handshake: cols[5].parse().unwrap(),
            received: cols[6].parse().unwrap(),
            sent: cols[7].parse().unwrap(),
        });

    }

}
